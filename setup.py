from langchain.llms import OpenAI

llm = OpenAI(model_name="text-davinci-003")

question = "What can i do with LangChain and OpenAI?"
print(llm(question))


from langchain.prompts import PromptTemplate
template = "What {num} resources cand i take to learn {language}?"
prompt = PromptTemplate(template=template, input_variables=['num', 'language'])
from langchain import LLMChain
chain = LLMChain(llm=llm, prompt=prompt)
input = {'num': 3, 'language': 'Python'}
print (chain.run(input))