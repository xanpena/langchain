# v.0.0.2 - OpenAPI SDK

- pip install langchain
- export OPENAI_API_KEY=""
- pip install --upgrade openai


# v.0.0.1 - Environment

- pip install -r requirements.txt
- pip freeze > requirements.txt
- source env/bin/activate
    - deactivate
- python3 -m venv env
- apt install python3.10-venv